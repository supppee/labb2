//
//  File.swift
//  labb2
//
//  Created by user162692 on 11/4/19.
//  Copyright © 2019 ham. All rights reserved.
//

import UIKit
class projectItem{
    let title:String
    let date:String
    let description:String
    let image:UIImage
    
    init(title:String,date:String,description:String,image:UIImage){
        self.title = title
        self.date = date
        self.description = description
        self.image = image
    }
}
