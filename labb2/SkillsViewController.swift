//
//  SkillsViewController.swift
//  labb2
//
//  Created by user162692 on 10/28/19.
//  Copyright © 2019 ham. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {

    @IBOutlet weak var animatedView: UIView!
    @IBOutlet weak var stopAnimatingButton: UIButton!
    @IBOutlet weak var bpmInput: UITextField!
    @IBOutlet weak var startAnimatingButton: UIButton!
    var isAnimating : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        bpmInput.keyboardType = UIKeyboardType.numberPad
        
        let tap = UITapGestureRecognizer(target:self.view,action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)
    }
    func animate(bpm: Int){
        let duration = Double(60/Double(bpm))
        print(duration)
        if(isAnimating){
            UIView.animate(withDuration: duration, delay:duration,
             animations: {
                 self.animatedView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
             },
             completion: { _ in
                UIView.animate(withDuration: 0) {
                     self.animatedView.transform = CGAffineTransform.identity
                 }
                self.animate(bpm:bpm)
             })
        }
    }
    @IBAction func dismissModal(_ sender: Any) {
        isAnimating = false
        dismiss(animated:true)
        
    }
    @IBAction func startAnimation(_ sender: Any) {
        isAnimating = true
        let bpm = Int(bpmInput.text!) ?? 60
        animate(bpm:bpm)
        stopAnimatingButton.isEnabled = true
        startAnimatingButton.setTitle("", for: .normal)
    }
    @IBAction func stopAnimatingPressed(_ sender: Any) {
        isAnimating = false
        stopAnimatingButton.isEnabled = false
        startAnimatingButton.setTitle("Start", for: .normal)

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
