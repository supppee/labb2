//
//  ExperienceDetailViewController.swift
//  labb2
//
//  Created by user162692 on 10/29/19.
//  Copyright © 2019 ham. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    
    @IBOutlet weak var workTitle: UILabel!
    @IBOutlet weak var workDate: UILabel!
    @IBOutlet weak var workImage: UIImageView!
    @IBOutlet weak var workDescription: UITextView!
    var project:projectItem?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        workTitle.text = project?.title
        workDate.text = project?.date
        workImage.image = project?.image
        workDescription.text = project?.description
        self.title = project?.title
    }

}
