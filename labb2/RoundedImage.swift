//
//  RoundedImage.swift
//  labb2
//
//  Created by user162692 on 11/6/19.
//  Copyright © 2019 ham. All rights reserved.
//

import UIKit

class RoundedImage: UIImageView{
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.height / 2
    }
}
