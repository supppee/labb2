//
//  WorkTableViewCell.swift
//  labb2
//
//  Created by user162692 on 11/4/19.
//  Copyright © 2019 ham. All rights reserved.
//

import UIKit


class WorkTableViewCell: UITableViewCell{
    
    @IBOutlet weak var workImage: UIImageView!
    @IBOutlet weak var workDate: UILabel!
    @IBOutlet weak var workTitle: UILabel!
}
