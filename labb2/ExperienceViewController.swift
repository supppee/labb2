//
//  ExperienceViewController.swift
//  labb2
//
//  Created by user162692 on 10/28/19.
//  Copyright © 2019 ham. All rights reserved.
//

import UIKit

class ExperienceViewController: UITableViewController {
    var sections = ["Work","School"]
    var projects = [
        [
            //Work
            projectItem(title:"Classified web project",date:"2019-",description:"Can't describe it as it's classified",image:#imageLiteral(resourceName: "webImg"))
        ],
        [
            //School
            projectItem(title:"Web project",date:"2019-2019",description:"A personal, pretty bad looking website i made in the course 'Web development fundamentals'",image:#imageLiteral(resourceName: "webImg")),
            projectItem(title:"iOS project",date:"2019-",description:"Haven't really done anything except planning yet...",image:#imageLiteral(resourceName: "iosImg")),
        ]
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projects[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath) as! WorkTableViewCell
        cell.workTitle.text = projects[indexPath.section][indexPath.row].title
        cell.workDate.text = projects[indexPath.section][indexPath.row].date
        cell.workImage.image = projects[indexPath.section][indexPath.row].image
        return cell
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ShowDetailView", sender: indexPath)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ExperienceDetailViewController, let indexPath = sender as? IndexPath{
            destination.project = projects[indexPath.section][indexPath.row]
        }
    }
        
}
	
